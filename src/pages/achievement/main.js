/*
 * @Description: In User Settings Edit
 * @Author: your name
 * @Date: 2019-09-11 08:58:55
 * @LastEditTime: 2019-09-11 09:03:52
 * @LastEditors: Please set LastEditors
 */
import Vue from 'vue'
import App from './index'

const app = new Vue(App)
app.$mount()